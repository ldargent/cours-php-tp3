<?php

namespace Exceptions;

/**
 * Class ArticleNotFoundException
 * @package Exceptions
 */
class ArticleNotFoundException extends \Exception
{
    /**
     * ArticleNotFoundException constructor.
     * 
     * @param string $id
     */
    public function __construct($id)
    {
        $message = "Article not found (id: $id)";

        parent::__construct($message);
    }
}